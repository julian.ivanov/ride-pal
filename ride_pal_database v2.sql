-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ride_pal_database
DROP DATABASE IF EXISTS `ride_pal_database`;
CREATE DATABASE IF NOT EXISTS `ride_pal_database` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ride_pal_database`;

-- Dumping structure for table ride_pal_database.albums
DROP TABLE IF EXISTS `albums`;
CREATE TABLE IF NOT EXISTS `albums` (
  `AlbumID` int(11) NOT NULL AUTO_INCREMENT,
  `AlbumName` varchar(1000) NOT NULL DEFAULT '0',
  `AlbumTrackListURL` varchar(1000) NOT NULL DEFAULT '0',
  `Active` tinyint(1) DEFAULT 1,
  `ArtistID` int(11) DEFAULT NULL,
  PRIMARY KEY (`AlbumID`),
  KEY `albums_artist_id_fk` (`ArtistID`),
  CONSTRAINT `albums_artist_id_fk` FOREIGN KEY (`ArtistID`) REFERENCES `artists` (`ArtistID`)
) ENGINE=InnoDB AUTO_INCREMENT=1422 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table ride_pal_database.albums_genres
DROP TABLE IF EXISTS `albums_genres`;
CREATE TABLE IF NOT EXISTS `albums_genres` (
  `album_genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`album_genre_id`),
  KEY `albums_genres_album_id_fk` (`album_id`),
  KEY `albums_genres_genre_id_fk` (`genre_id`),
  CONSTRAINT `albums_genres_album_id_fk` FOREIGN KEY (`album_id`) REFERENCES `albums` (`AlbumID`),
  CONSTRAINT `albums_genres_genre_id_fk` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`GenreID`)
) ENGINE=InnoDB AUTO_INCREMENT=1359 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table ride_pal_database.artists
DROP TABLE IF EXISTS `artists`;
CREATE TABLE IF NOT EXISTS `artists` (
  `ArtistID` int(11) NOT NULL AUTO_INCREMENT,
  `ArtistName` varchar(1000) NOT NULL,
  `TrackListURL` varchar(1000) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`ArtistID`)
) ENGINE=InnoDB AUTO_INCREMENT=1416 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table ride_pal_database.authorities
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `username_authority` (`username`,`authority`),
  CONSTRAINT `authorities_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table ride_pal_database.genres
DROP TABLE IF EXISTS `genres`;
CREATE TABLE IF NOT EXISTS `genres` (
  `GenreID` int(11) NOT NULL AUTO_INCREMENT,
  `GenreName` varchar(50) NOT NULL,
  `Active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`GenreID`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table ride_pal_database.playlists
DROP TABLE IF EXISTS `playlists`;
CREATE TABLE IF NOT EXISTS `playlists` (
  `PlaylistID` int(11) NOT NULL AUTO_INCREMENT,
  `PlaylistTitle` varchar(50) NOT NULL,
  `Playtime` int(11) NOT NULL,
  `Rank` decimal(10,2) NOT NULL,
  `UserID` varchar(50) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`PlaylistID`),
  KEY `FK_playlists_users` (`UserID`),
  CONSTRAINT `playlists_user_id_fk` FOREIGN KEY (`UserID`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table ride_pal_database.playlists_genres
DROP TABLE IF EXISTS `playlists_genres`;
CREATE TABLE IF NOT EXISTS `playlists_genres` (
  `playlists_genres_id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`playlists_genres_id`),
  KEY `playlists_genres_playlist_id_fk` (`playlist_id`),
  KEY `playlists_genres_genre_id_fk` (`genre_id`),
  CONSTRAINT `playlists_genres_genre_id_fk` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`GenreID`),
  CONSTRAINT `playlists_genres_playlist_id_fk` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`PlaylistID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table ride_pal_database.playlists_tracks
DROP TABLE IF EXISTS `playlists_tracks`;
CREATE TABLE IF NOT EXISTS `playlists_tracks` (
  `playlist_track_id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) DEFAULT NULL,
  `track_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`playlist_track_id`),
  KEY `playlist_track_playlist_id_fk` (`playlist_id`),
  KEY `playlists_tracks_track_id_fk` (`track_id`),
  CONSTRAINT `playlist_track_playlist_id_fk` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`PlaylistID`),
  CONSTRAINT `playlists_tracks_track_id_fk` FOREIGN KEY (`track_id`) REFERENCES `tracks` (`TrackID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table ride_pal_database.tracks
DROP TABLE IF EXISTS `tracks`;
CREATE TABLE IF NOT EXISTS `tracks` (
  `TrackID` int(11) NOT NULL AUTO_INCREMENT,
  `ArtistID` int(11) DEFAULT NULL,
  `Title` varchar(1000) NOT NULL,
  `AlbumID` int(11) DEFAULT NULL,
  `Link` varchar(1000) NOT NULL,
  `PreviewURL` varchar(1000) DEFAULT NULL,
  `Duration` int(11) NOT NULL,
  `Rank` double(10,2) NOT NULL,
  `Active` tinyint(1) DEFAULT 1,
  `Genre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`TrackID`),
  KEY `FK_tracks_artists` (`ArtistID`),
  KEY `FK_tracks_albums` (`AlbumID`),
  CONSTRAINT `tracks_album_id_fk` FOREIGN KEY (`AlbumID`) REFERENCES `albums` (`AlbumID`),
  CONSTRAINT `tracks_artist_id_fk` FOREIGN KEY (`ArtistID`) REFERENCES `artists` (`ArtistID`)
) ENGINE=InnoDB AUTO_INCREMENT=1378 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table ride_pal_database.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(68) NOT NULL,
  `enabled` tinyint(4) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
