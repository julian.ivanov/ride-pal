-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ride_pal_database
DROP DATABASE IF EXISTS `ride_pal_database`;
CREATE DATABASE IF NOT EXISTS `ride_pal_database` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ride_pal_database`;

-- Dumping structure for table ride_pal_database.albums
DROP TABLE IF EXISTS `albums`;
CREATE TABLE IF NOT EXISTS `albums` (
                                        `AlbumID` int(11) NOT NULL AUTO_INCREMENT,
                                        `AlbumName` varchar(50) NOT NULL DEFAULT '0',
                                        `AlbumTrackListURL` varchar(1000) NOT NULL DEFAULT '0',
                                        `Active` tinyint(1) DEFAULT 0,
                                        PRIMARY KEY (`AlbumID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ride_pal_database.albums: ~0 rows (approximately)
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;

-- Dumping structure for table ride_pal_database.artists
DROP TABLE IF EXISTS `artists`;
CREATE TABLE IF NOT EXISTS `artists` (
                                         `ArtistID` int(11) NOT NULL AUTO_INCREMENT,
                                         `ArtistName` varchar(50) NOT NULL,
                                         `TrackListURL` varchar(1000) NOT NULL,
                                         `Active` tinyint(1) DEFAULT NULL,
                                         PRIMARY KEY (`ArtistID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ride_pal_database.artists: ~0 rows (approximately)
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;

-- Dumping structure for table ride_pal_database.authorities
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
                                             `username` varchar(50) NOT NULL,
                                             `authority` varchar(50) NOT NULL,
                                             UNIQUE KEY `username_authority` (`username`,`authority`),
                                             CONSTRAINT `authorities_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ride_pal_database.authorities: ~4 rows (approximately)
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
REPLACE INTO `authorities` (`username`, `authority`) VALUES
('misho', 'ROLE_USER'),
('nadya', 'ROLE_USER'),
('pesho', 'ROLE_ADMIN'),
('pesho', 'ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;

-- Dumping structure for table ride_pal_database.genres
DROP TABLE IF EXISTS `genres`;
CREATE TABLE IF NOT EXISTS `genres` (
                                        `GenreID` int(11) NOT NULL AUTO_INCREMENT,
                                        `GenreName` varchar(50) NOT NULL,
                                        `Active` tinyint(1) DEFAULT NULL,
                                        PRIMARY KEY (`GenreID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table ride_pal_database.genres: ~22 rows (approximately)
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
REPLACE INTO `genres` (`GenreID`, `GenreName`, `Active`) VALUES
(1, 'All', NULL),
(2, 'Pop', NULL),
(3, 'Rap/Hip Hop', NULL),
(4, 'Rock', NULL),
(5, 'Dance', NULL),
(6, 'R&B', NULL),
(7, 'Alternative', NULL),
(8, 'Electro', NULL),
(9, 'Folk', NULL),
(10, 'Reggae', NULL),
(11, 'Jazz', NULL),
(12, 'Classical', NULL),
(13, 'Films/Games', NULL),
(14, 'Metal', NULL),
(15, 'Soul & Funk', NULL),
(16, 'African Music', NULL),
(17, 'Asian Music', NULL),
(18, 'Blues', NULL),
(19, 'Brazilian Music', NULL),
(20, 'Indian Music', NULL),
(21, 'Kids', NULL),
(22, 'Latin Music', NULL);
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;

-- Dumping structure for table ride_pal_database.playlists
DROP TABLE IF EXISTS `playlists`;
CREATE TABLE IF NOT EXISTS `playlists` (
                                           `PlaylistID` int(11) NOT NULL AUTO_INCREMENT,
                                           `PlaylistTitle` varchar(50) NOT NULL,
                                           `Playtime` int(11) NOT NULL,
                                           `Rank` decimal(10,2) NOT NULL,
                                           `UserID` varchar(50) DEFAULT NULL,
                                           `Active` tinyint(1) DEFAULT NULL,
                                           PRIMARY KEY (`PlaylistID`),
                                           KEY `FK_playlists_users` (`UserID`),
                                           CONSTRAINT `playlists_user_id_fk` FOREIGN KEY (`UserID`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ride_pal_database.playlists: ~0 rows (approximately)
/*!40000 ALTER TABLE `playlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlists` ENABLE KEYS */;

-- Dumping structure for table ride_pal_database.playlists_genres
DROP TABLE IF EXISTS `playlists_genres`;
CREATE TABLE IF NOT EXISTS `playlists_genres` (
                                                  `playlists_genres_id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `playlist_id` int(11) DEFAULT NULL,
                                                  `genre_id` int(11) DEFAULT NULL,
                                                  PRIMARY KEY (`playlists_genres_id`),
                                                  KEY `playlists_genres_playlist_id_fk` (`playlist_id`),
                                                  KEY `playlists_genres_genre_id_fk` (`genre_id`),
                                                  CONSTRAINT `playlists_genres_genre_id_fk` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`GenreID`),
                                                  CONSTRAINT `playlists_genres_playlist_id_fk` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`PlaylistID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ride_pal_database.playlists_genres: ~0 rows (approximately)
/*!40000 ALTER TABLE `playlists_genres` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlists_genres` ENABLE KEYS */;

-- Dumping structure for table ride_pal_database.playlists_tracks
DROP TABLE IF EXISTS `playlists_tracks`;
CREATE TABLE IF NOT EXISTS `playlists_tracks` (
                                                  `playlist_track_id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `playlist_id` int(11) DEFAULT NULL,
                                                  `track_id` int(11) DEFAULT NULL,
                                                  PRIMARY KEY (`playlist_track_id`),
                                                  KEY `playlist_track_playlist_id_fk` (`playlist_id`),
                                                  KEY `playlists_track_id_fk` (`track_id`),
                                                  CONSTRAINT `playlist_track_playlist_id_fk` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`PlaylistID`),
                                                  CONSTRAINT `playlists_track_id_fk` FOREIGN KEY (`track_id`) REFERENCES `tracks` (`TrackID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ride_pal_database.playlists_tracks: ~0 rows (approximately)
/*!40000 ALTER TABLE `playlists_tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlists_tracks` ENABLE KEYS */;

-- Dumping structure for table ride_pal_database.tracks
DROP TABLE IF EXISTS `tracks`;
CREATE TABLE IF NOT EXISTS `tracks` (
                                        `TrackID` int(11) NOT NULL AUTO_INCREMENT,
                                        `ArtistID` int(11) NOT NULL,
                                        `Title` varchar(50) NOT NULL,
                                        `AlbumID` int(11) NOT NULL,
                                        `Link` varchar(50) NOT NULL,
                                        `PreviewURL` varchar(50) DEFAULT NULL,
                                        `Duration` int(11) NOT NULL,
                                        `Rank` double(10,2) NOT NULL,
                                        `Active` tinyint(1) DEFAULT NULL,
                                        PRIMARY KEY (`TrackID`),
                                        KEY `FK_tracks_artists` (`ArtistID`),
                                        KEY `FK_tracks_albums` (`AlbumID`),
                                        CONSTRAINT `FK_tracks_albums` FOREIGN KEY (`AlbumID`) REFERENCES `albums` (`AlbumID`),
                                        CONSTRAINT `FK_tracks_artists` FOREIGN KEY (`ArtistID`) REFERENCES `artists` (`ArtistID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ride_pal_database.tracks: ~0 rows (approximately)
/*!40000 ALTER TABLE `tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracks` ENABLE KEYS */;

-- Dumping structure for table ride_pal_database.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
                                       `username` varchar(50) NOT NULL,
                                       `password` varchar(68) NOT NULL,
                                       `enabled` tinyint(4) NOT NULL,
                                       PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ride_pal_database.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`username`, `password`, `enabled`) VALUES
('misho', '{noop}pass3', 1),
('nadya', '{noop}pass2', 1),
('pesho', '{noop}pass1', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
