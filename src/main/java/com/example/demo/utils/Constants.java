package com.example.demo.utils;

public class Constants {

    public static final String MESSAGE_ID_POSITIVE_OR_ZERO = "ID should be zero or a positive number";
    public static final String MESSAGE_PLAYTIME_POSITIVE = "Playtime should be zero or a positive number";
    public static final String MESSAGE_DURATION_POSITIVE = "Duration should be zero or a positive number";
    public static final String MESSAGE_RANK_POSITIVE = "Rank should be zero or a positive number";
    public static final String MESSAGE_NAME_SIZE = "Name should contain between 1 and 50 characters";
    public static final String MESSAGE_TITLE_SIZE = "Title should contain between 1 and 50 characters";
    public static final String MESSAGE_URL_SIZE = "URL should contain between 1 and 1000 characters";
    public static final String MESSAGE_LINK_SIZE = "Link should contain between 1 and 1000 characters";
    public static final String MESSAGE_PREVIEW_SIZE = "Preview should contain between 1 and 1000 characters";
    public static final String MESSAGE_USERNAME_SIZE = "Username should contain at least one character";
    public static final String MESSAGE_PASSWORD_SIZE = "Password should contain at least one character";


}
