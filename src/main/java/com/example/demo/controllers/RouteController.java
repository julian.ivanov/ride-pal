package com.example.demo.controllers;


import com.example.demo.models.DTOs.PlaylistDTO;
import com.example.demo.models.DTOs.PlaylistData;
import com.example.demo.models.DTOs.PlaylistDataForm;
import com.example.demo.models.DtoMapper;
import com.example.demo.models.Playlist;
import com.example.demo.services.contracts.GenreService;
import com.example.demo.services.contracts.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class RouteController {

    private GenreService genreService;
    private PlaylistService playlistService;

    @Autowired
    public RouteController(GenreService genreService, PlaylistService playlistService){
        this.genreService = genreService;
        this.playlistService = playlistService;
    }


    @GetMapping("/generate")
    public String generatePlaylistPage(Model model){

        List<PlaylistData> playlistDataList = genreService.getActiveGenres().stream().map(g -> new PlaylistData(g.getName())).collect(Collectors.toList());

        model.addAttribute("playListDataEmptyObject",new PlaylistData());
        model.addAttribute("form", new PlaylistDataForm(playlistDataList) {
        });

        return "contact-us";
    }

    @PostMapping("/generate")
    public String createPlaylist(@ModelAttribute PlaylistDataForm form, Principal principal){

        PlaylistDTO playlistDTO = DtoMapper.toPlaylistDTO(form,principal.getName());

        Playlist playlist =  playlistService.createPlaylist(playlistDTO);

        return "redirect:/user/playlists";
    }

}
