package com.example.demo.controllers;

import com.example.demo.models.DTOs.GenreDTO;
import com.example.demo.models.Genre;
import com.example.demo.models.User;
import com.example.demo.services.contracts.GenreService;
import com.example.demo.services.contracts.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SyncController {

    private GenreService genreService;
    private TrackService trackService;

    @Autowired
    public SyncController(GenreService genreService,TrackService trackService) {
        this.genreService = genreService;
        this.trackService = trackService;
    }

    @GetMapping("/admin/sync_genres")
    public String syncGenreView(Model model){
        model.addAttribute("genres", genreService.getAllGenres());
        model.addAttribute("hasNewGenres", false);
        model.addAttribute("newGenres", new ArrayList<Genre>());
        return "sync_genres";

    }

    @GetMapping("/admin/{genreId}/activate")
    public String activateGenre(@PathVariable int genreId){

        genreService.changeGenreStatus(genreId, true);

        return "redirect:/admin/sync_genres";
    }

    @GetMapping("/admin/{genreId}/deactivate")
    public String deactivateGenre(@PathVariable int genreId){

        genreService.changeGenreStatus(genreId, false);
        return "redirect:/admin/sync_genres";
    }

    @GetMapping("/admin/sync")
        public String syncGenres(Model model){

            try {
                model.addAttribute("genres", genreService.getAllGenres());
                List<GenreDTO> newGenres = genreService.syncGenres();
                boolean hasNewGenres = false;
                if(newGenres != null && newGenres.size() > 0){
                    hasNewGenres = true;
                }
                model.addAttribute("hasNewGenres", hasNewGenres);
                model.addAttribute("newGenres", newGenres);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return "sync_genres";
    }

    @GetMapping("/admin/{genreID}/tracks")
    public String syncTracksPerGenre(@PathVariable int genreID, Model model){
            Genre genre = genreService.getById(genreID);
        try {
            trackService.syncTracksPerGenre(genre);
        } catch (IOException e) {
            model.addAttribute("error", "Error in communication with the external API");
        }

        return "redirect:/admin/sync_genres";
    }
}
