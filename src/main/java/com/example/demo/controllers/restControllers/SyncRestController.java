package com.example.demo.controllers.restControllers;

import com.example.demo.services.contracts.GenreService;
import com.example.demo.services.contracts.TrackService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class SyncRestController {

    private GenreService genreService;
    private TrackService trackService;

    @Autowired
    public SyncRestController(GenreService genreService, TrackService trackService) {
        this.genreService = genreService;
        this.trackService = trackService;
    }

    @GetMapping("/syncGenres")
    public String syncGenres() {
        try {
            genreService.syncGenres();
        } catch (IOException e) {

            //TODO Handle exception
        }
        return "Genres created successfully.";
    }

    @GetMapping("/syncTracks")
    public String syncTracks() {
        try {
            trackService.syncTracksForAllGenres();
        } catch (IOException e) {

            //TODO Handle exception
        }
        return "Tracks created successfully.";
    }

    @GetMapping("/count/{genre}")
    public long countTracksPerGenre(@PathVariable String genre) {
        return trackService.countTracksPerGenre(genre);
    }


    @GetMapping("/syncNews")
    public Map<String,String> getLatestNews(){
            return trackService.getLatestNews();
    }
}
