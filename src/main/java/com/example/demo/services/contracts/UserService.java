package com.example.demo.services.contracts;

import com.example.demo.models.DTOs.UserDTO;
import com.example.demo.models.User;

import java.util.List;

public interface UserService {

    User createUser(User user);

    UserDTO getByName(String username);

    List<UserDTO> getAllUsersDtos();

    UserDTO editUserProfile(UserDTO userDto);

    void changePassword(String username, String newPassword);

    UserDTO makeAdmin(UserDTO userDto);

    UserDTO removeAdmin(UserDTO userDto);

    User changeUserStatus(String userID, boolean b);
}
