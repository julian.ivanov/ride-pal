package com.example.demo.services;

import com.example.demo.models.DTOs.UserDTO;
import com.example.demo.models.DtoMapper;
import com.example.demo.models.Role;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.RoleRepository;
import com.example.demo.repositories.contracts.UserRepository;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    private UserRepository userRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private RoleRepository roleRepository;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserDetailsManager userDetailsManager,
                           PasswordEncoder passwordEncoder, RoleRepository roleRepository){
        this.userRepository = userRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }


    @Override
    public User createUser(User user) {
        return userRepository.createUser(user);
    }

    @Override
    public UserDTO getByName(String username) {
        User user = userRepository.getByName(username);
        return DtoMapper.toDto(user);
    }

    @Override
    public List<UserDTO> getAllUsersDtos() {
        List<User> allUsers = userRepository.getAllUsers();
        List<UserDTO> userDtos = allUsers.stream().map(u -> DtoMapper.toDto(u)).collect(Collectors.toList());
        return userDtos;
    }

    @Override
    public UserDTO editUserProfile(UserDTO userDto) {
        User user = DtoMapper.fromDto(userDto);
        User userFromDB = userRepository.getByName(userDto.getUsername());
        user.setPassword(userFromDB.getPassword());
        user.setRoles(userFromDB.getRoles());
        return DtoMapper.toDto(userRepository.updateUserDetails(user));

    }

    @Override
    public void changePassword(String username, String newPassword) {
        User user = userRepository.getByName(username);
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.updateUserDetails(user);
    }

    @Override
    public UserDTO makeAdmin(UserDTO userDto) {
        User user = userRepository.getByName(userDto.getUsername());

        Role role = roleRepository.getRole(ROLE_ADMIN);
        user.addRole(role);
        return DtoMapper.toDto(userRepository.updateUserDetails(user));
    }
    @Override
    public UserDTO removeAdmin(UserDTO userDto) {
        User user = userRepository.getByName(userDto.getUsername());

        Role role = roleRepository.getRole(ROLE_ADMIN);
        user.removeRole(role);
        return DtoMapper.toDto(userRepository.updateUserDetails(user));
    }

    @Override
    public User changeUserStatus(String userID, boolean b) {
        User user = userRepository.getByName(userID);
        user.setEnabled(b);

        User userFromDB = userRepository.updateUserDetails(user);

        return userFromDB;

    }
}
