package com.example.demo.models.DTOs;

public class GenreDTO {

    private int id;
    private String name;
    private int deezerGenreId;
    private boolean active;
    private int tracksCount;

    public GenreDTO() {
    }

    public GenreDTO(int id, String name, int deezerGenreId, boolean active) {
        this.id = id;
        this.name = name;
        this.deezerGenreId = deezerGenreId;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDeezerGenreId() {
        return deezerGenreId;
    }

    public void setDeezerGenreId(int deezerGenreId) {
        this.deezerGenreId = deezerGenreId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getTracksCount() {
        return tracksCount;
    }

    public void setTracksCount(int tracksCount) {
        this.tracksCount = tracksCount;
    }
}
