package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Genre;
import com.example.demo.models.Playlist;
import com.example.demo.repositories.contracts.PlaylistRepository;
import com.example.demo.services.contracts.GenreService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Repository
public class PlaylistRepositoryImpl implements PlaylistRepository {

    private SessionFactory sessionFactory;
    private GenreService genreService;

    @Autowired
    public PlaylistRepositoryImpl(SessionFactory sessionFactory, GenreService genreService){
        this.sessionFactory = sessionFactory;
        this.genreService = genreService;
    }


    @Override
    public Playlist createPlaylist(Playlist playlist) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(playlist);

            session.getTransaction().commit();

            return playlist;
        }
    }

    @Override
    public List<Playlist> createPlaylist(List<Playlist> playlistList) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            for (int i = 0; i < playlistList.size(); i++) {
                session.save(playlistList.get(i));
            }
            session.getTransaction().commit();

            return playlistList;
        }
    }

    @Override
    public Playlist getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Playlist playlist = session.get(Playlist.class, id);
            if(playlist == null){
                throw new EntityNotFoundException(String.format("Playlist with id '%d' does not exist.",id));
            }
            return playlist;
        }
    }

    @Override
    public List<Playlist> getAllPlaylists() {
        try(Session session = sessionFactory.openSession()){
            Query<Playlist> playlistQuery = session.createQuery("from Playlist where active = true",Playlist.class);

            if(playlistQuery.list().size()==0){
                return Collections.emptyList();
            } else {
                return playlistQuery.list();
            }
        }
    }

    @Override
    public List<Playlist> getAllPlaylists(Map<Integer, String> flagMapMVC) {
        try(Session session = sessionFactory.openSession()){
            Query<Playlist> playlistQuery = buildQuery(flagMapMVC,session);

            List<Playlist> playlistList = playlistQuery.list();

            if(flagMapMVC.containsKey(1) && playlistList.size()!=0){

                playlistList = filterGenres(playlistList,flagMapMVC.get(1));
            }

            if(playlistList.size()==0){
                return Collections.emptyList();
            } else {
                return playlistList;
            }
        }
    }

    private List<Playlist> filterGenres(List<Playlist> playlistList, String genreName) {
        Genre genre = genreService.getGenreByName(genreName);
        List<Playlist> filtered = playlistList.stream()
                .filter(genres -> genres.getGenres().contains(genre))
                .collect(Collectors.toList());

        return filtered;
    }

    private Query<Playlist> buildQuery(Map<Integer, String> flagMapMVC, Session session) {
        StringBuffer queryString = new StringBuffer("from Playlist where title <> :title and active = true");
        boolean filterDuration = flagMapMVC.containsKey(2);

        if(filterDuration){
            queryString.append(" and playtime <= :playtime");
        }

        if(flagMapMVC.containsKey(0)){
            queryString.append(" order by rank");
        }

        Query<Playlist> playlistQuery = session.createQuery(String.valueOf(queryString),Playlist.class);
        playlistQuery.setParameter("title","");

        if(filterDuration){
            playlistQuery.setParameter("playtime", (Integer.parseInt(flagMapMVC.get(2))));
        }

        return playlistQuery;
    }

    @Override
    public List<Playlist> getPlaylistsByUserID(String name) {
        try(Session session = sessionFactory.openSession()){

            Query<Playlist> playlistQuery = session.createQuery("from Playlist where userID = :userID and title <> :title and active = true",Playlist.class);
            playlistQuery.setParameter("userID",name);
            playlistQuery.setParameter("title","");

            if(playlistQuery.list().size() ==0){
                return Collections.emptyList();
            } else {
                return playlistQuery.list();
            }
        }
    }

    @Override
    public int[] getMinAndMaxDuration() {
        try(Session session = sessionFactory.openSession()){
            int[] minAndMaxDuration = new int[2];
            Query<Integer> minDurationQuery = session.createQuery("select min(playtime) from Playlist where title <> :title and active = true", Integer.class);
            minDurationQuery.setParameter("title","");
            minAndMaxDuration[0] = (int)((double)minDurationQuery.list().get(0)*0.0166666667);

            Query<Integer> maxDurationQuery = session.createQuery("select  max(playtime) from Playlist where title <> :title and active = true",Integer.class);
            maxDurationQuery.setParameter("title","");
            minAndMaxDuration[1] = (int)((double)maxDurationQuery.list().get(0)*0.0166666667);

            return minAndMaxDuration;
        }
    }

    @Override
    public List<Playlist> getAllPlaylistsAdmin() {
        try(Session session = sessionFactory.openSession()){
            Query<Playlist> playlistQuery = session.createQuery("from Playlist where title <> :title",Playlist.class);
            playlistQuery.setParameter("title","");

            if(playlistQuery.list().size()==0){
                return Collections.emptyList();
            } else {
                return playlistQuery.list();
            }
        }
    }

    @Override
    public Playlist updatePlaylist(Playlist playlist) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(playlist);
            session.getTransaction().commit();

            return playlist;
        }
    }
}
