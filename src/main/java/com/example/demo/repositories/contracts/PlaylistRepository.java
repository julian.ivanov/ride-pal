package com.example.demo.repositories.contracts;

import com.example.demo.models.Playlist;

import java.util.List;
import java.util.Map;

public interface PlaylistRepository {

    Playlist createPlaylist(Playlist playlist);

    List<Playlist> createPlaylist(List<Playlist> playlistList);

    Playlist getById(int id);

    List<Playlist> getAllPlaylists();

    List<Playlist> getAllPlaylists(Map<Integer,String> flagMapMVC);

    List<Playlist> getPlaylistsByUserID(String name);

    int[] getMinAndMaxDuration();

    List<Playlist> getAllPlaylistsAdmin();

    Playlist updatePlaylist(Playlist playlist);
}
