package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Genre;
import com.example.demo.models.Track;
import com.example.demo.repositories.contracts.TrackRepository;
import com.example.demo.utils.JSONutils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Repository
public class TrackRepositoryImpl implements TrackRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TrackRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void createTrack(Track track,String genre) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            track.setGenre(genre);
            session.save(track);

            session.getTransaction().commit();
        }
    }

    @Override
    public List<Track> createTrack(List<Track> trackList) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            for (int i = 0; i < trackList.size(); i++) {
                session.save(trackList.get(i));
            }
            session.getTransaction().commit();

            return trackList;
        }
    }

    @Override
    public Track getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Track track = session.get(Track.class, id);
            if(track == null){
                throw new EntityNotFoundException(String.format("Track with id '%d' does not exist.",id));
            }
            return track;
        }
    }

    @Override
    public List<Track> getAllTracks() {
        try(Session session = sessionFactory.openSession()){
            Query<Track> trackQuery = session.createQuery("from Track ",Track.class);

            return trackQuery.list();
        }
    }

    @Override
    public boolean checkTrackExists(String trackName, String previewUrl) {
        try(Session session = sessionFactory.openSession()){
            Query<Track> trackQuery = session.createQuery("from Track where title = :title and preview = :preview",Track.class);
            trackQuery.setParameter("title",trackName);
            trackQuery.setParameter("preview",previewUrl);
            if(trackQuery.list().size()!=0){
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public long countTracksPerGenre(String genre) {
        try(Session session = sessionFactory.openSession()) {
            Query<Long> countQuery = session.createQuery("select count(*) from Track t where t.genre = :genre", Long.class);
            countQuery.setParameter("genre", genre);
            return countQuery.getSingleResult();
        }
    }

    @Override
    public List<Track> getTracksByGenre(String genre) {
        try(Session session = sessionFactory.openSession()){
            Query<Track> trackQuery = session.createQuery("from Track where genre = :genre",Track.class);
            trackQuery.setParameter("genre",genre);

            return trackQuery.list();
        }
    }

    @Override
    public List<Track> getTopTracksByGenre(String genre) {
        try(Session session = sessionFactory.openSession()){
            Query<Track> trackQuery = session.createQuery("from Track where genre = :genre order by rank asc",Track.class);
            trackQuery.setParameter("genre",genre);


            return trackQuery.list();
        }
    }

    @Override
    public List<Track> getTracksByGenreAndTime(String genre, int timeRemaining) {
        try(Session session = sessionFactory.openSession()){
            Query<Track> trackQuery = session.createQuery("from Track  where genre = :genre and duration <= :timeRemaining",Track.class);
            trackQuery.setParameter("genre",genre);
            trackQuery.setParameter("timeRemaining",timeRemaining);

            if(trackQuery.list().size() == 0){
                return Collections.emptyList();
            }

            return trackQuery.list();
        }
    }
}
