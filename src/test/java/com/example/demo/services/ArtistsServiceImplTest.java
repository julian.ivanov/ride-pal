package com.example.demo.services;

import com.example.demo.models.Album;
import com.example.demo.models.Artist;
import com.example.demo.repositories.contracts.AlbumRepository;
import com.example.demo.repositories.contracts.ArtistRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)

public class ArtistsServiceImplTest {

    @Mock
    ArtistRepository artistRepository;

    @InjectMocks
    ArtistServiceImpl artistService;




    @Test
    public void createArtist_ShouldNot_CreateArtist_When_ArtistExists() {
        // Arrange
        Artist artist = MockFactory.mockArtist1();

        Mockito.when(artistRepository.checkArtistExists(artist.getName())).thenReturn(Boolean.TRUE);

        // Act
        artistService.createArtist(artist);

        // Assert
        Mockito.verify(artistRepository, Mockito.times(0)).createArtist(artist);

    }


    @Test
    public void createArtistList_Should_ReturnArtistList_When_ArtistListIsCreated() {
        // Arrange
        List<Artist> artistList = MockFactory.mockArtistList();

        Mockito.when(artistRepository.createArtist(artistList)).thenReturn(artistList);

        // Act
        List<Artist> returnedArtistList = artistService.createArtist(artistList);

        // Assert
        Assert.assertEquals(artistList.get(0), returnedArtistList.get(0));
        Assert.assertEquals(artistList.get(1), returnedArtistList.get(1));

    }

    @Test
    public void getArtistById_Should_ReturnArtist_When_ArtistExists() {

        // Arrange
        Artist artist = MockFactory.mockArtist1();

        Mockito.when(artistRepository.getById(ArgumentMatchers.anyInt())).thenReturn(artist);

        // Act
        Artist returnedArtist = artistService.getById(1);

        // Assert
        Assert.assertEquals(returnedArtist.getId(), artist.getId());
    }


    @Test
    public void checkArtistExists_Should_True_WhenArtistExists() {
        // Arrange
        Artist artist = MockFactory.mockArtist1();
        Mockito.when(artistRepository.checkArtistExists(artist.getName())).thenReturn(true);

        // Act
        boolean returnedBoolean = artistService.checkArtistExists(artist.getName());

        // Assert
        Assert.assertTrue(returnedBoolean);

    }

    @Test
    public void getAllArtists_Should_ReturnAllArtists_When_Prompted() {

        // Arrange
        List<Artist> artistList = MockFactory.mockArtistList();

        Mockito.when(artistRepository.getAllArtists()).thenReturn(artistList);

        // Act
        List<Artist> returnedArtistList = artistService.getAllArtists();

        // Assert
        Assert.assertEquals(returnedArtistList.get(0), artistList.get(0));
        Assert.assertEquals(returnedArtistList.get(1), artistList.get(1));

    }

    @Test
    public void getByName_Should_ReturnArtist_When_ArtistExists() {

        // Arrange
        Artist artist = MockFactory.mockArtist1();

        Mockito.when(artistRepository.getArtistByName(artist.getName())).thenReturn(artist);

        // Act
        Artist returnedArtist = artistService.getArtistByName("artist 1");

        // Assert
        Assert.assertEquals(returnedArtist.getName(), artist.getName());
    }



}
