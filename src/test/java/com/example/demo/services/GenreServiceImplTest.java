package com.example.demo.services;

import com.example.demo.models.DTOs.GenreDTO;
import com.example.demo.models.Genre;
import com.example.demo.repositories.contracts.GenreRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class GenreServiceImplTest {

    @Mock
    GenreRepository genreRepository;

    @InjectMocks
    GenreServiceImpl genreService;

    @Test
    public void test_adding_count_tracks_to_genre_list() {

        //Arrange
        Genre genre1 = MockFactory.mockGenre1();
        Genre genre2 = MockFactory.mockGenre2();
        List<Genre> genreList = MockFactory.mockGenreList();

        Mockito.when(genreRepository.getAllGenres()).thenReturn(genreList);

//        Mockito.when(genreRepository.countTracksInGenre("genre 1")).thenReturn(23);
//        Mockito.when(genreRepository.countTracksInGenre("genre 2")).thenReturn(48);

        //Act
        List<GenreDTO> allGenres = genreService.getAllGenres();

        Assert.assertNotEquals(23, allGenres.get(0).getTracksCount());


    }

    @Test
    public void createGenre_Should_ReturnGenre_When_GenreIsCreated() {
        // Arrange
        Genre genre = MockFactory.mockGenre1();

        Mockito.when(genreRepository.createGenre(genre)).thenReturn(genre);

        // Act
        Genre returnedGenre = genreService.createGenre(genre);

        // Assert
        Assert.assertEquals(returnedGenre.getId(), genre.getId());

    }

    @Test
    public void createGenreList_Should_ReturnGenreList_When_GenreListIsCreated() {
        // Arrange
        List<Genre> genreList = MockFactory.mockGenreList();

        Mockito.when(genreRepository.createGenres(genreList)).thenReturn(genreList);

        // Act
        List<Genre> returnedGenreList = genreService.createGenres(genreList);

        // Assert
        Assert.assertEquals(genreList.get(0), returnedGenreList.get(0));

    }

    @Test
    public void getByID_Should_ReturnGenre_When_GenreExists() {

        // Arrange
        Genre genre = MockFactory.mockGenre1();

        Mockito.when(genreRepository.getById(ArgumentMatchers.anyInt()))
                .thenReturn(genre);

        // Act
        Genre returnedGenre = genreService.getById(1);

        // Assert
        Assert.assertEquals(returnedGenre.getId(), genre.getId());
    }

    @Test
    public void getActiveGenres_Should_Return_ActiveGenres() {
        // Arrange
        List<Genre> genreList = MockFactory.mockGenreList();

        Mockito.when(genreRepository.getActiveGenres()).thenReturn(genreList);

        // Act
        List<Genre> returnedGenreList = genreService.getActiveGenres();

        // Assert
        Assert.assertEquals(genreList.get(0), returnedGenreList.get(0));
    }

    @Test
    public void checkGenreExists_Should_True_WhenGenreExists() {
        // Arrange
        Genre genre = MockFactory.mockGenre1();
        Mockito.when(genreRepository.checkGenreExists(genre.getName())).thenReturn(true);

        // Act
        boolean returnedBoolean = genreService.checkGenreExists(genre.getName());

        // Assert
        Assert.assertTrue(returnedBoolean);

    }

    @Test
    public void getByName_Should_ReturnGenre_When_BeerExists() {

        // Arrange
        Genre genre = MockFactory.mockGenre1();

        Mockito.when(genreRepository.getGenreByName(genre.getName())).thenReturn(genre);

        // Act
        Genre returnedGenre = genreService.getGenreByName("genre 1");

        // Assert
        Assert.assertEquals(returnedGenre.getId(), genre.getId());
    }

//    @Test
//    public void syncGenres_Should_SyncGenres_When_Activated() {
//        // Arrange
//        List<GenreDTO> genreDTOList = MockFactory.mockGenreListDTO();
//
//        Mockito.when(genreRepository.getActiveGenres()).thenReturn(genreList);
//
//        // Act
//        List<Genre> returnedGenreList = genreService.getActiveGenres();
//
//        // Assert
//        Assert.assertEquals(genreList.get(0), returnedGenreList.get(0));
//
//    }
//    @Test
//    public void getAllGenres_Should_ReturnAllGenres() {
//        // Arrange
//        List<GenreDTO> genreListDTO = MockFactory.mockGenreListDTO();
//
//        Mockito.when(genreRepository.getAllGenres()).thenReturn(genreListDTO));
//
//        // Act
//        List<Genre> returnedGenreList = genreService.getActiveGenres();
//
//        // Assert
//        Assert.assertEquals(genreList.get(0), returnedGenreList.get(0));
//    }

}


