package com.example.demo.services;

import com.example.demo.models.Album;
import com.example.demo.models.Artist;
import com.example.demo.models.Genre;
import com.example.demo.models.Track;
import com.example.demo.repositories.contracts.GenreRepository;
import com.example.demo.repositories.contracts.TrackRepository;
import com.example.demo.services.contracts.AlbumService;
import com.example.demo.services.contracts.ArtistService;
import com.example.demo.services.contracts.GenreService;
import com.example.demo.utils.JSONutils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TrackServiceImplTest {

    @Mock
    TrackRepository trackRepository;

    @Mock
    GenreRepository genreRepository;

    @Mock
    GenreService genreService;

    @InjectMocks
    TrackServiceImpl trackService;

    @Mock
    TrackServiceImpl trackServiceMock;

    @Mock
    AlbumService albumService;

    @Mock
    ArtistService artistService;


    public void createTrackList_Should_ReturnTrackList_When_TrackListIsCreated() {
        // Arrange
        List<Track> trackList = MockFactory.mockTrackList();

        Mockito.when(trackRepository.createTrack(trackList)).thenReturn(trackList);

        // Act
        List<Track> returnedTrackList = trackService.createTrack(trackList);

//        // Assert
        Assert.assertEquals(trackList.get(0), returnedTrackList.get(0));
        Assert.assertEquals(trackList.get(1), returnedTrackList.get(1));

    }

    @Test
    public void getTrackById_Should_ReturnTrack_When_TrackExists() {

        // Arrange
        Track track = MockFactory.mockTrack1();

        Mockito.when(trackRepository.getById(ArgumentMatchers.anyInt())).thenReturn(track);

        // Act
        Track returnedTrack = trackService.getById(1);

        // Assert
        Assert.assertEquals(returnedTrack.getId(), track.getId());
    }


    @Test
    public void getAllTracks_Should_ReturnAllTracks_When_Prompted() {

        // Arrange
        List<Track> trackList = MockFactory.mockTrackList();

        Mockito.when(trackRepository.getAllTracks()).thenReturn(trackList);

        // Act
        List<Track> returnedTrackList = trackService.getAllTracks();

        // Assert
        Assert.assertEquals(returnedTrackList.get(0), trackList.get(0));
        Assert.assertEquals(returnedTrackList.get(1), trackList.get(1));

    }

    @Test
    public void checkTrackExists_Should_ReturnTrue_WhenTrackExists() {
        // Arrange
        Track track = MockFactory.mockTrack1();
        Mockito.when(trackRepository.checkTrackExists(track.getTitle(), track.getPreview())).thenReturn(true);

        // Act
        boolean returnedBoolean = trackService.checkTrackExists(track.getTitle(), track.getPreview());

        // Assert
        Assert.assertTrue(returnedBoolean);

    }

//    @Test
//    public void test_syncTracksForAllGenres() throws IOException {
//        // Arrange
//        List<Genre> genreList = MockFactory.mockGenreList();
//        Mockito.when(genreService.getActiveGenres()).thenReturn(genreList);
//
//        // Act
//        trackService.syncTracksForAllGenres();
//
//        // Assert
//        Mockito.verify(trackServiceMock,
//                Mockito.times(3)).syncTracksPerGenre(ArgumentMatchers.any(Genre.class));
//    }

    @Test
    public void test_retrieve_album_when_doesnt_exists(){
        Album album = new Album();
        album.setTitle("album title");
        Track track = MockFactory.mockTrack1();
        track.setAlbum(album);

        Mockito.when(albumService.checkAlbumExists(ArgumentMatchers.anyString())).thenReturn(Boolean.FALSE);

        //Act
        trackService.createTrackAlbum(track, "Pop");


        Mockito.verify(albumService, Mockito.times(1)).createAlbum(ArgumentMatchers.any(Album.class));
    }


    @Test
    public void test_retrieve_album_when_exists(){

        //Arrange

        Album album = new Album();
        album.setTitle("album title");
        Track track = MockFactory.mockTrack1();
        track.setAlbum(album);

        Mockito.when(albumService.checkAlbumExists(ArgumentMatchers.anyString())).thenReturn(Boolean.TRUE);

        //Act
        trackService.createTrackAlbum(track, "Pop");

        //Assert
        Mockito.verify(albumService, Mockito.times(1)).getAlbumByName(ArgumentMatchers.anyString());
    }

    @Test
    public void test_retrieve_artist_when_exists(){
        Artist artist = new Artist();
        artist.setName("great artist");
        Track track = MockFactory.mockTrack1();
        track.setArtist(artist);

        Mockito.when(artistService.checkArtistExists(ArgumentMatchers.anyString())).thenReturn(Boolean.TRUE);
        //Act
        trackService.createTrackArtist(track);

        //Assert
        Mockito.verify(artistService, Mockito.times(1)).getArtistByName(ArgumentMatchers.anyString());
    }

    @Test
    public void test_retrieve_artist_when_doesnt_exists(){
        Artist artist = new Artist();
        artist.setName("great artist");
        Track track = MockFactory.mockTrack1();
        track.setArtist(artist);

        Mockito.when(artistService.checkArtistExists(ArgumentMatchers.anyString())).thenReturn(Boolean.FALSE);
        //Act
        trackService.createTrackArtist(track);

        //Assert
        Mockito.verify(artistService, Mockito.times(1)).createArtist(ArgumentMatchers.any(Artist.class));
    }


    @Test
    public void createTrack_Should_CreateTrack_When_TrackIsCreated(){
        //Arrange
        Track track = MockFactory.mockTrack1();
        Genre genre = MockFactory.mockGenre1();
        String name = genre.getName();

        //Act
        trackService.createTrack(track,name);

        Mockito.verify(trackRepository, Mockito.times(1)).createTrack(track,name);
    }

    @Test
    public void createTrack_Should_Return_TrackList(){
        //Arrange
        List<Track> tracks = MockFactory.mockTrackList();

        Mockito.when(trackRepository.createTrack(tracks)).thenReturn(tracks);

        //Act
        List<Track> returned = trackService.createTrack(tracks);

        //Assert

        Assert.assertEquals(tracks.size(),returned.size());
    }

    @Test
    public void getTracksByGenre_Should_GetTracks(){
        //Arrange
        List<Track> tracks = MockFactory.mockTrackList();
        Genre genre = MockFactory.mockGenre1();

        Mockito.when(trackRepository.getTracksByGenre(genre.getName())).thenReturn(tracks);

        //Act
        List<Track> tracks1 = trackService.getTracksByGenre(genre.getName());

        //Assert
        Assert.assertEquals(tracks.size(),tracks1.size());
    }

    @Test
    public void getTopTracksByGenre_Should_GetTracks(){
        //Arrange
        List<Track> tracks = MockFactory.mockTrackList();
        Genre genre = MockFactory.mockGenre1();

        Mockito.when(trackRepository.getTopTracksByGenre(genre.getName())).thenReturn(tracks);

        //Act
        List<Track> tracks1 = trackService.getTopTracksByGenre(genre.getName());

        //Assert
        Assert.assertEquals(tracks.size(),tracks1.size());
    }

    @Test
    public void countTracksPerGenre_Should_Count_Tracks(){
        //Arrange
        Genre genre = MockFactory.mockGenre1();

        Mockito.when(trackRepository.countTracksPerGenre(genre.getName())).thenReturn(10L);

        //Act
        long count = trackService.countTracksPerGenre(genre.getName());

        //Assert
        Assert.assertEquals(count,10);

    }

    @Test
    public void getTracksByGenreAndTime_Should_GetTracks(){
        //Arrange
        List<Track> tracks = MockFactory.mockTrackList();
        Genre genre = MockFactory.mockGenre1();

        Mockito.when(trackRepository.getTracksByGenreAndTime(genre.getName(),100)).thenReturn(tracks);

        //Act
        List<Track> tracks1 = trackService.getTracksByGenreAndTime(genre.getName(),100);

        //Assert
        Assert.assertEquals(tracks.size(),tracks1.size());
    }


}
